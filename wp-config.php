<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', 'wp-01' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'root' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'localhost:8889' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R$eH=7*fUz{ObzB2|x9Dvvdg&lF:iBna{W=;M=weDa3t:WQbr;>8M.r7XWq>I}r7' );
define( 'SECURE_AUTH_KEY',  'i$IXhzeF#,sN65,8w`8>mz$aPP?g%CF3,=q7!w}V57n52wlPdBKO/mC$tj{<8=6a' );
define( 'LOGGED_IN_KEY',    'XKIS~MN8E,6JzBBsnRCGd)UR[Tq|qdH6;4M]2~RWn.b`B9XiB5+dcx[8^Rhn)^<p' );
define( 'NONCE_KEY',        '8a`kwVW:RNK_`gC4U`lJFmj@][Ew6ufLfdVMj2X0E|)rJNspbUGtcco~bzQ5.jVc' );
define( 'AUTH_SALT',        '0K_Eow#VrHugzt,7qaO~ia(2vRlJ~Z)y6Te6H!4&$1tS8_=fET%:FVwOZbm@>;_Q' );
define( 'SECURE_AUTH_SALT', 'J=:sbh~v{;Uktpm~|k1=Rrw#<;#f(Kf]M,KVX5)odwCQO-_.guzP8eSTX-Xsb24Q' );
define( 'LOGGED_IN_SALT',   '?<{W8{K^Ol},B*z+[SnQ7t`)+A`{?8}adg?[pi5!z+}ivwKJg#fl:G_dyCUiuRvY' );
define( 'NONCE_SALT',       'DL.Xsf%Ylj{R2J/3;xvp,#mvY%SoNTu%>/r/Mgx1]nt,8?Vq6FeV*!uP0d.,P*)M' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
